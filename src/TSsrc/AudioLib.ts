/*----------------------------------------------------------------------------------------------------------
::“Copyright 2018 Clayton Burnett” 
::This program is distributed under the terms of the GNU General Public License
------------------------------------------------------------------------------------------------------------*/
/**
 * @fileOverview
 *
 * This file contains the implementation for the Audio controller
 *
 * @author Clayton Burnett <the82professional@hotmail.com>
 */
 
/**
 * ###############################################################################################################
 *                                              AudioLib
 */
/**
 * @class
 * Class modeling an Audio controller
 *
 * @description
 * This is the interface for interacting with the page audio controls.  Contains all playing audio
 **/
 /**
 * @constructor
 */
export class AudioLib{
	private _PlayingAudio: AudioTrack[];
	private _Channels:  HTMLAudioElement[];
	private _FreeChannels: HTMLAudioElement[];
	private _UsedChannels: HTMLAudioElement[];
	private _ClipTimeout: number;
	private _ClipCache: AudioTrack[];
	private _Matches: AudioTrack[];
	private _AudioCap: number;
	private _includer: AudioTrack;

    constructor(ADChannels: NodeListOf<HTMLAudioElement>, Clips: AudioTrack[], ClipMax?: number){
		this._AudioCap = ClipMax | 6; //The maximum number of concurrently playing audio
		this._PlayingAudio = [];
		this._FreeChannels = Array.prototype.slice.call(ADChannels);
		this._UsedChannels = [];
	    this._ClipTimeout = 10000;
		this._ClipCache = Clips;
		this._Matches = [];
		this._Channels = Array.prototype.slice.call(ADChannels);
    }
    //----------------------------------------------SET METHODS--------------------------------------------------
    /**
    * Sets the list of available page audio clips
    * @param {AudioTrack[]} Clips An array of current page audio clips
    */
    set AudioClips(Clips: AudioTrack[]){
        if(Clips.length >= 0){
            this._ClipCache = Clips;
        }
    }
    /**
    * Sets the list of available page audio channels that the controller should manage and binds event handlers
    * @param {NodeListOf<HTMLAudioElement>} Channels The Nodelist of Audio Channels to Manage
    */
    set AvailableChannels(Channels: NodeListOf<HTMLAudioElement>){
		   if(Channels.length >= 0){
			   	//Convert to Array and Store
				this._Channels =  Array.prototype.slice.call(Channels);
		   }
	}
    //----------------------------------------------GET METHODS-------------------------------------------------
	/**
    * Get the Array of playing audio channels
    * @returns {AudioTrack[]} Returns the Array of playing Audio Channels
    */
    get PlayingAudio(): AudioTrack[]{
        return(this._PlayingAudio);
	}
	//--------------------PRIVATE INTERNAL FUNCTIONS---------------------------
	/**
	* checks to make sure there is a free channel and reserves it
	* @returns {HTMLAudioElement} Returns a reference to a free Audio Element
	*/
	private GetFreeChannel(): HTMLAudioElement{
		if(this._FreeChannels.length > 0){
			//There Are channels available, allocate
			let channel = this._FreeChannels.pop();
			this._UsedChannels.push(channel);
			return(channel);
		}else{
			console.log("Allocation Error: No more free channels")
		}
	}
	/**
    * Remove an element from the playing items
   	* @param {string} ClipName The name of the clip
	* @param {number} ClipIndex A unique clip index for playing the same clip simultaneously
    */
   	private RemovePlaying(ClipName: string, ClipIndex: number): void{
		for(let index in this._PlayingAudio){
			if(this._PlayingAudio[index].AudioFile == ClipName && this._PlayingAudio[index].ID == ClipIndex){
				//We have a match; remove it
				this._PlayingAudio.splice(Number(index), 1);
			}
		}
	}	
	/**
    * Scan and find out if the Audio element is playing on a channel
   	* @param {string} ClipName The name of the clip
	* @param {number} ClipIndex A unique clip index
    * @returns {AudioTrack[]} A list of playing items matching the filters
    */
   	private GetPlayingItems(ClipName: string, ClipIndex: number): AudioTrack[]{
		let Matches = new Array();
		for(let index of this._PlayingAudio){
			if(index.AudioFile == ClipName && index.ID == ClipIndex){
				//Match
				Matches.push(index);
			}
		}
		return(Matches);
	}	
	/**
	* Add the AudioTrack to the array of playing Audio
	* @param {AudioTrack} Audio Audio element to add to the list of playing audio tracks
	*/
	private AddToPlaying(Audio: AudioTrack): void{
		if(Audio.AudioFile.length != 0){
			this._PlayingAudio.push(Audio);
		}
	}
	/**
	* Stop all playing audio and reset the Audio Controller to a neutral state
	*/
	private Clean(): void{
		//Stop all playing Audio
		this._Channels.forEach(function(e){
			e.pause;
			e.src = "";
		}, this);	
		this._PlayingAudio = null;
		this._FreeChannels = null;
		this._UsedChannels = null;
	    this._ClipTimeout = null;
        this._ClipCache = null;
	}
	//--------------------PUBLIC INTERFACE FUNCTIONS-----------------
	/**
	* Skips to the current time value in seconds given by "Value"
	* @param {string} ClipName The name of the clip
	* @param {number} ID A unique clip ID
	* @param {number} Value The track starting position in seconds
	*/
	public SkipTo(ClipName: string, ID: number, Value: number): void{
		let found = 0;
		//Look for first instance
		for(let i of this._PlayingAudio){
			if(i.AudioFile == ClipName && i.ID == ID){
				//We have a match; add to matches
				found = 1;
				//set seek
				i.TrackPos = Value;
				
			}
		}
		//After creating match db tally up matches
		if(found == 0){
			//No Matches Found
			console.log("No matches found in SkipTo");
		}
	}
	/**
	* Play a clip from a RadiusAudio object
	* @param {string} ClipName The name of the clip
	* @param {number} ID A unique clip index for playing the same clip simultaneously
	*/
	public PlayAudio(ClipName: string, ID: number): void{
		let exist = this.GetPlayingItems(ClipName, ID);
		//1st thing check for a duplicate entry 
		if(exist.length == 0 && (this._PlayingAudio.length + 1) <= this._AudioCap){
			//Get a free channel remember to release when done
			let Channel: HTMLAudioElement = this.GetFreeChannel();
			let found = false;
			//0 indicates no channels free
			for(let i of this._ClipCache){
				if(i.AudioFile == ClipName && i.ID == ID){
					this.AddToPlaying(i);
					Channel.src = i.AudioFile;
					Channel.play();
					found = true;
				}
			}
			if(found == false){
				console.log("No audio file matching Name: " + ClipName + " And ID: " + ID);
			}
		}else{
			//Channels full
			if((this._PlayingAudio.length + 1) == this._AudioCap){
				//When we are out of channels
				console.log("Cannot play track, no free channels.  Please free one in the site implementation");
			}
		}
	}
	/**
	* Stops audio associated with the object on the associated controller
	* @param {string} ClipName The name of the clip
	* @param {number} ID A unique clip ID
	*/
	public Stop(ClipName: string, ID: number): void{
		let PlayingChannels = this._UsedChannels;
		let PlayingAudio = this.GetPlayingItems(ClipName, ID);
		//Only stop if there are matches
		if(PlayingAudio.length > 0){
			for(let i of PlayingChannels){
				//Make sure the audio is registered to a channel and playing
				if(i.src == ClipName && i.paused != true){
					i.pause;
					//Remove the clip from the list
					this.RemovePlaying(ClipName, ID);
					//Don't forget to stop the clip on the channel
					var found = false;
					this._Channels.forEach(function(element){
						if(element.src == ClipName && found == false){
							//Just remove the first one encountered
							element.pause();
							element.currentTime = 0;
							found = true;
						}})
					console.log("stopped one audio track");
				}
			}
		}else{
			console.log("Could Not Stop audio clip because there were no matches");
		}
	}
	/**
	 * Frees a channel given the 0 indexed channel number
	 * @param {number} index The index of the channel to free
	 */
	 public FreeChannel(index: number): void{
		if(this._UsedChannels.length != 0){
			if(this._UsedChannels[index]){
				let channel = this._UsedChannels[index]; 
				this._UsedChannels.splice(Number(index), 1);
				this._FreeChannels.push(channel);
			}else{
				console.log("in FreeChannel: No channel at specified index");
			}
		}else{
			console.log("No channels to free");
		}
	}
}
export interface AudioTrack{
    readonly AudioFile: string;
    RepeatingFlag?: boolean;
    Volume?: number;
    readonly Duration: number;
    readonly Format: string;
    Playing?: boolean;
    SavedTrack?: number;
    ID: number;
	TrackPos: number;
	channel?: HTMLAudioElement;
}
